package rgu.ac.uk.cosinesimilarity;

/**
 *
 * @author Jérémie Clos <j.clos@rgu.ac.uk>
 */
public final class CosineSimilarity  {

    public static float getSimilarity(final TermFreqVector string1, final TermFreqVector string2) {
        float cosineSimilarity = 0f;
        double dotProduct = 0d;
        double magnitudeStr1 = 0d;
        double magnitudeStr2 = 0d;
        double[][] commonBasis = TermFreqVector.getCommonBasis(string1, string2);
        for (int i = 0; i < commonBasis[0].length ; i ++) {
            dotProduct += commonBasis[0][i] * commonBasis[1][i] ;
            magnitudeStr1 += Math.pow(commonBasis[0][i], 2);
            magnitudeStr2 += Math.pow(commonBasis[1][i], 2);
        }
        double magnitude1 = Math.sqrt(magnitudeStr1);
        double magnitude2 = Math.sqrt(magnitudeStr2);
        if (magnitude1 != 0.0 | magnitude2 != 0.0) {
            cosineSimilarity = (float) (dotProduct / (magnitude1 * magnitude2));
        } else {
            return 0f;
        }
        return cosineSimilarity;
    }
}



