/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rgu.ac.uk.cosinesimilarity;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Jérémie Clos <j.clos@rgu.ac.uk>
 */
public final class TermFreqVector {
    private final Map<String, Double> vector ;
    
    public TermFreqVector() {
        vector = new HashMap<>();
    }
    
    public int size() {
        int size = 0 ;
        for (double d : vector.values()) size += d ;
        return size ;
    }
    
    /**
     * Create a term vector with the default frequency provided. If the same term appears
     * N times, it will end up with a frequency of N*defaultFrequency.
     * @param defaultFrequency
     * @param terms 
     */
    public TermFreqVector(double defaultFrequency, String ... terms){
        vector = new HashMap<>();
        for (String term : terms) {
            this.addTerm(term, defaultFrequency);
        }
    }
    
    public TermFreqVector(double defaultFrequency, List<String> terms){
        vector = new HashMap<>();
        for (String term : terms) {
            this.addTerm(term, defaultFrequency);
        }
    }
    
    public void addTerm(String term, double freq) {
        vector.put(term, vector.getOrDefault(term, 0d) + freq);
    }
    
    public double get(String term) {
        return vector.getOrDefault(term, 0d);
    }
    
    public boolean contains(String term) {
        return vector.containsKey(term);
    }
    
    public String[] getTerms() {
        return vector.keySet().toArray(new String[vector.size()]);
    }
    
    public Double[] getFrequencies() {
        return vector.values().toArray(new Double[vector.size()]);
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, Double> entry : vector.entrySet()) {
            sb.append(entry.getValue()).append(" ");
        }
        return sb.toString().trim() ;
    }
    
    static public double[][] getCommonBasisAndPrint(TermFreqVector v1, TermFreqVector v2) {
        return commonBasis(v1, v2, true);
    }
    
    static public double[][] getCommonBasis(TermFreqVector v1, TermFreqVector v2) {
        return commonBasis(v1, v2, false);
    }
    
    static private double[][] commonBasis(TermFreqVector v1, TermFreqVector v2, boolean print) {
        //figure out total # of terms
        Set<String> terms = new HashSet<>();
        Map<String, Integer> fTerms = new HashMap<>();
        Map<Integer, String> rTerms = new HashMap<>();
        
        for (String termV1 : v1.getTerms()) {
            terms.add(termV1);
        }
        for (String termV2 : v2.getTerms()) {
            terms.add(termV2);
        }
        int i = 0 ;
        for (String term : terms) {
            fTerms.put(term, i);
            rTerms.put(i, term);
            i++;
        }
        
        int sizeVector = fTerms.size() ;
        double[][] result = new double[2][sizeVector];
        
        for (int j = 0 ; j < result[0].length ; j ++) {
            String term = rTerms.get(j);
            double freqV1 = v1.get(term);
            double freqV2 = v2.get(term);
            result[0][j] = freqV1 ;
            result[1][j] = freqV2 ;
        }
        
        if (print) {
            for (int x = 0 ; x < result[0].length ; x ++) {
                    System.out.print(result[0][x] + " ");
            }
            System.out.println();
            for (int x = 0 ; x < result[1].length ; x ++) {
                    System.out.print(result[1][x] + " ");
            }
            System.out.println();  
        }

        return result ;
    }
}
